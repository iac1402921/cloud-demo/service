#-----------
## Create image octavia-amphora-haproxy-2024.1
#-----------

resource "openstack_images_image_v2" "octavia-amphora-haproxy-2024-1" {
  name             = "octavia-amphora-haproxy-2024.1"
  image_source_url = "https://swift.services.a.regiocloud.tech/swift/v1/AUTH_b182637428444b9aa302bb8d5a5a418c/openstack-octavia-amphora-image/octavia-amphora-haproxy-2024.1.qcow2"
  container_format = "bare"
  disk_format      = "qcow2"
  min_disk_gb      = "2"
  min_ram_mb       = "1024"
  visibility       = "private"
  tags             = ["amphora-img"]

  properties = {
    os_version    = "22.04"
    os_distro     = "ubuntu"
    hw_disk_bus   = "scsi"
    hw_scsi_model = "virtio-scsi"
  }
}

#-----------
## Create image Fedora-CoreOS-38
#-----------

resource "openstack_images_image_v2" "Fedora-CoreOS-38" {
  name             = "Fedora-CoreOS-38"
  image_source_url = "https://file.ap.com/fedora-coreos-38.20230806.3.0-openstack.x86_64.qcow2"
  container_format = "bare"
  disk_format      = "qcow2"
  visibility       = "public"

  properties = {
    os_version = "38.20230806.3.0"
    os_distro  = "fedora-coreos"
  }
}
