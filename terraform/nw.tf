#-----------
## Create ext-network
#-----------
resource "openstack_networking_network_v2" "ext-network" {
  name           = "ext-network"
  admin_state_up = "true"
  external       = "true"
  shared         = "true"
  segments {
    network_type     = "flat"
    physical_network = "physnet1"
  }
}

#-----------
## Create ext-subnet
#-----------
resource "openstack_networking_subnet_v2" "ext-subnet" {
  name            = "ext-subnet"
  network_id      = openstack_networking_network_v2.ext-network.id
  cidr            = "192.168.2.0/23"
  gateway_ip      = "192.168.2.1"
  dns_nameservers = ["192.168.2.254", "192.168.5.254"]
  enable_dhcp     = "false"
  allocation_pool {
    start = "192.168.3.180"
    end   = "192.168.3.209"
  }
}
