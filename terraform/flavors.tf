#-----------
## Create flavor STD-2-4-10
#-----------

resource "openstack_compute_flavor_v2" "STD-2-4-10" {
  description = "Standard flavor for k8s VM"
  name        = "STD-2-4-10"
  vcpus       = "2"
  ram         = "4096"
  disk        = "10"
  swap        = "0"
  ephemeral   = "0"
  is_public   = "true"

  extra_specs = {
    ":architecture"             = "x86_architecture",
    ":category"                 = "compute_optimized",
    "quota:disk_total_iops_sec" = "2000",
    "hw:numa_nodes"             = "1",
    "hw:mem_page_size"          = "any"
  }
}

#-----------
## Create flavor STD-2-4-5
#-----------

resource "openstack_compute_flavor_v2" "STD-2-4-5" {
  description = "Standard flavor for DB VM"
  name        = "STD-2-4-5"
  vcpus       = "2"
  ram         = "4096"
  disk        = "5"
  swap        = "0"
  ephemeral   = "0"
  is_public   = "true"

  extra_specs = {
    ":architecture"             = "x86_architecture",
    ":category"                 = "compute_optimized",
    "quota:disk_total_iops_sec" = "2000",
    "hw:numa_nodes"             = "1",
    "hw:mem_page_size"          = "any"
  }
}
