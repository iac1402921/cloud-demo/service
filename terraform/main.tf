terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }
  }
}

# Провайдер полностью настраивается через переменные окружения OS_
provider "openstack" {
}

provider "dns" {
  update {
    server = "ad.ap.com"
    gssapi {
      realm    = "AP.COM"
      username = var.dns_username
      password = var.dns_password
    }
  }
}
